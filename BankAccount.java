import java.util.concurrent.locks.*;

public class BankAccount
{
    private double balance;

    Lock lock;
    Condition condition;

    public BankAccount()
    {
        balance = 0;
	lock = new ReentrantLock();
	condition = lock.newCondition();
	
    }

    public void deposit(double amount)
    {
	lock.lock();
        System.out.print("Depositing " + amount);
        double newBalance = balance + amount;
        System.out.println(", new balance is " + newBalance);
        balance = newBalance;
	condition.signalAll();
	lock.unlock();
	
    }

    public void withdraw(double amount)
    {
	lock.lock();
	
	try{
	    while(amount > balance)
		condition.await();
	}catch(InterruptedException ex){
	    System.out.println("Error");
	}
	
        System.out.print("Withdrawing " + amount);
        double newBalance = balance - amount;
        System.out.println(", new balance is " + newBalance);
        balance = newBalance;
	lock.unlock();
    }

    public double getBalance()
    {
        return balance;
    }
}
